# Changelog

| Version | Date       | Description                                                  |
| ------- | ---------- | ------------------------------------------------------------ |
| 2.0.1   | 2024-07-08 | Add py.typed; make probe.serial a non-None string            |
| 2.0.0   | 2024-06-24 | Convert battery and RSSI levels into sensors                 |
| 1.1.0   | 2024-06-24 | Change battery level from enum to percentage                 |
| 1.0.0   | 2024-06-13 | First production release                                     |
| 0.0.1   | 2024-05-15 | Initial version                                              |

