# Development

Clone project:

```shell
$ git clone git@gitlab.com:sensoterra/public/python.git sensoterra-python
$ cd sensoterra-python
$ python3 -m venv .
```

Activate virtual environment:

```shell
$ . bin/activate
```

Install requirements (needed only once):

```shell
$ pip install -r requirements.txt
```

## During development

Format code:

```shell
$ black src
$ isort src
```

Validate code:

```shell
$ mypy src
```

Test code:

```shell
$ read -p "Email: " EMAIL
$ read -sp "Password: " PASSWORD; echo
$ export EMAIL PASSWORD
$ pytest
```

## Update packages

```shell
$ pip list --outdated | awk '{if(NR>=3) print $1}'
$ pip list --outdated | awk '{if(NR>=3) print $1}' | xargs -n1 pip3 install -U
$ pip freeze > requirements.txt
```

## Build to test

Build package into `dist`:

```shell
$ vi pyproject.toml CHANGELOG.md
$ git st
$ git commit -a
$ git tag -l
$ git tag -a v0.0.0
$ rm dist/*
$ python -m build
```

Upload to TestPyPi:

```shell
$ twine upload --repository testpypi dist/*
```

## Upload to production

Upload to PyPi:

```shell
$ twine check dist/*
$ twine upload dist/*
```

